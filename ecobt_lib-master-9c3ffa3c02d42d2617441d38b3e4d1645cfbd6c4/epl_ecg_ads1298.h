 /**
 * 
 * @ingroup Module
 * @defgroup ECG
 * @{
 * @file epl_acc_lis331dl.h
 * 
 *
 * @author GuoHuei
 * @date 2011/2/25
 * @copyright 
 * Copyright (c) 2013 EPLAB National Tsing Hua University . All rights reserved.\n
 * The information contained herein is confidential property of NTHU. 
 * The material may be used for a personal and non-commercial use only in connection with 
 * a legitimate academic research purpose.  
 * Any attempt to copy, modify, and distribute any portion of this source code or derivative thereof for commercial, 
 * political, or propaganda purposes is strictly prohibited.  
 * All other uses require explicit written permission from the authors and copyright holders. 
 * This copyright statement must be retained in its entirety and displayed 
 * in the copyright statement of derived source code or systems.
 */
 
#ifndef ADS1298_H_
#define ADS1298_H_

#include "hal_mcu.h"
#include "hal_types.h"
#include "epl_hal_spi.h"

/** @cond */
#define ADS1298_START     P2_2
#define ADS1298_CS        P1_4
#define ADS1298_DRDY      P2_1
#define ADS1298_DIN       P1_6
#define ADS1298_DOUT      P1_7
#define ADS1298_CLK       P1_5

#define HAL_ADS1298_DRDY_BIT            0x02
#define ADS1298_READY     (ADS1298_DRDY == 1)

#define ECG_OUTPUT_LEN    27
/** @endcond */

/** @struct ADS1298_RegMap_t
*	@brief	An struct describing register map in ads1298.
*/
/** @union config
*	@brief Contains data and register map in ads1298.
*/
/** @struct RegMap_t
*	@brief	An struct describing register map in ads1298.				 					
*/
typedef struct{
  union{
    uint8 data[26];
    struct{
      uint8 ID;         /**< ID control register 		*/   
      uint8 CONFIG1;    /**< Configuration register 1 	*/
      uint8 CONFIG2;    /**< Configuration register 2 	*/
      uint8 CONFIG3;    /**< Configuration register 3 	*/
      uint8 LOFF;       /**< Lead-off control register	*/
      uint8 CH1SET;     /**< Channel 1 Settings 		*/
      uint8 CH2SET;     /**< Channel 2 Settings 		*/
      uint8 CH3SET;     /**< Channel 3 Settings 		*/
      uint8 CH4SET;     /**< Channel 4 Settings 		*/
      uint8 CH5SET;     /**< Channel 5 Settings 		*/
      uint8 CH6SET;     /**< Channel 6 Settings 		*/
      uint8 CH7SET;     /**< Channel 7 Settings 		*/
      uint8 CH8SET;     /**< Channel 8 Settings 		*/
      uint8 RLD_SENSP;  /**< This register controls the selection of the positive signals from each channel for right leg drive derivation 				*/
      uint8 RLD_SENSN;  /**< This register controls the selection of the negative signals from each channel for right leg drive derivation 				*/
      uint8 LOFF_SENSP;	/**< This register selects the positive side from each channel for lead-off detection 											*/
      uint8 LOFF_SENSN; /**< This register selects the negative side from each channel for lead-off detection 											*/
      uint8 LOFF_FLIP;  /**< This register controls the direction of the current used for lead-off derivation 											*/
      uint8 LOFF_STATP; /**< This register stores the status of whether the positive electrode on each channel is on or of								*/
      uint8 LOFF_STATN; /**< This register stores the status of whether the negative electrode on each channel is on or off 							*/
      uint8 GPIO;       /**< The General-Purpose I/O Register controls the action of the three GPIO pins 												*/
      uint8 PACE;       /**< This register provides the PACE controls that configure the channel signal used to feed the external PACE detect circuitry */
      uint8 RESP;       /**< This register provides the controls for the respiration circuitry 															*/
      uint8 CONFIG4;    /**< Configuration register 4 																									*/
      uint8 WCT1;       /**< The WCT1 control register configures the device WCT circuit channel selection and the augmented leads 						*/
      uint8 WCT2;       /**< The WCT2 configuration register configures the device WCT circuit channel selection					 					*/
    } RegMap;
  }config;
}ADS1298_RegMap_t;


/** @union ADS1298_OPCODE
*	@brief Contains data and register map in ads1298.
*/
typedef enum {
	ADS129X_WAKEUP    = 0x02, /**< Wake-up from standby mode							*/
    ADS129X_STANDBY   = 0x04, /**< Enter standby mode									*/
    ADS129X_RESET     = 0x06, /**< Reset the device										*/
    ADS129X_START     = 0x08, /**< Start/restart (synchronize) conversions				*/
    ADS129X_STOP      = 0x0a, /**< Stop conversion										*/
	ADS129X_RDATAC    = 0x10, /**< Enable Read Data Continuous mode						*/
    ADS129X_SDATAC    = 0x11, /**< Stop Read Data Continuously mode						*/
    ADS129X_RDATA     = 0x12, /**< Read data by command; supports multiple read back	*/
	ADS129X_RREG      = 0x20, /**< Read n nnnn registers starting at address r rrrr		*/
    ADS129X_WREG      = 0x40, /**< Write n nnnn registers starting at address r rrrr	*/
	ADS129X_ADDR_MASK = 0x1f  /**< address mask											*/
}ADS1298_OPCODE;

// ADS1298 functions
/** @brief Setup SPI for ads1298.
 */
void ADS1298_init();

/* If you use the ADS129X_RESET, you must wait extra 14CLK(7196ns) */
/** @brief Send command to asd1298
*	@param[in] opcode 
*		The opcode send to ads1298
*/ 
void ADS1298_SEND_COMMAND(ADS1298_OPCODE opcode);

/** @brief Write byte(s) to specified address.
*	@param[in] opcode 
*		The opcode send to ads1298
*/ 
void ADS1298_WREG(unsigned char addr, unsigned char * payload, unsigned char len);

/** @brief Read byte(s) from specified address.
*	@param[in] opcode 
*		The opcode send to ads1298
*/ 
void ADS1298_RREG(unsigned char addr, unsigned char * payload, unsigned char len);

/** @brief Set function pointer.
*	@param[in] *function(void) 
*		Set a function pointer with void type input
*/ 
void ADS1298_set_FunctionCall(void (*function)(void));

// ECG functions
/** @brief Configure asd1298 for ECG function
*/ 
void ECG_init(void);

/** @brief Reset asd1298 for ECG function
*/ 
void ECG_reset(void);


/** @brief Read ECG function configuration from asd1298
*	@param [out] *config1
*		Pointer for upper 13 bytes configuration data 
*	@param [out] *config2
*		Pointer for lower 13 bytes configuration data 
*/ 
void ECG_getConfig(uint8 *config1, uint8 *config2);

/** @brief Write ECG function configuration to asd1298
*	@param [in] *config1
*		Pointer for upper 13 bytes configuration data 
*	@param [in] *config2
*		Pointer for lower 13 bytes configuration data 
*/ 
void ECG_setConfig(uint8 *config1, uint8 *config2);

/** @brief Load ECG data
*/ 
void ECG_load(void);

/** @brief Start ECG function
*/ 
void ECG_start(void);

/** @brief Stop ECG function
*/ 
void ECG_stop(void);

//interrupt routines
/** @brief Set function pointer.
*	@param[in] *function(uint8 *data) 
*		Set a function pointer with uint8 type input
*/ 
void ADS1298_set_Callback(void (*p)(uint8 *data));

//interrupt routines
/** @brief Set ISR.
*	@param[in] ECGEvent
*		ECG event type
*/ 
void halProcessECGInterrupt( uint8 ECGEvent );
//void ADS1298_DRDY_ISR(void) interrupt INTERRUPT_IPF;

#endif /* ADS1298_H_ */

/**
@}
*/
