 /**	
 *  @ingroup HAL
 *	@defgroup SD_Card
 *	@{
 *	@file 	epl_hal_sdcard.h
 *	@brief 	create software I2C to write or read
 *	
 *	@author	
 *	@date	
 *  @copyright Copyright 2013 EPLAB National Tsing Hua University. All rights reserved.\n
 *  The information contained herein is confidential property of NTHU. 
 *      The material may be used for a personal and non-commercial use only in connection with 
 *      a legitimate academic research purpose.  
 *      Any attempt to copy, modify, and distribute any portion of this source code or derivative thereof for commercial, 
 *      political, or propaganda purposes is strictly prohibited.  
 *      All other uses require explicit written permission from the authors and copyright holders. 
 *      This copyright statement must be retained in its entirety and displayed 
 *      in the copyright statement of derived source code or systems.
 */



#ifndef EPL_HAL_SDCARD_H
#define EPL_HAL_SDCARD_H

#ifdef __cplusplus
extern "C"
{
#endif

/***************************************************************************
***************************************************************************/
#include <ioCC2540.h>
#include <hal_types.h>
#include <hal_defs.h>

/* Memory card sector size */
#define SECTOR_SIZE 512
#define MAX_NUM_PARTITIONS 4

#define R1_ILL_COMMAND 2

/**
 * MMC/SD card SPI mode commands
 **/
#define CMD0 0x40	/**< software reset 											*/
#define CMD1 0x41	/**< brings card out of idle state                              */
#define CMD2 0x42	/**< not used in SPI mode                                       */
#define CMD3 0x43	/**< not used in SPI mode                                       */
#define CMD4 0x44	/**< not used in SPI mode                                       */
#define CMD5 0x45	/**< Reserved                                                   */
#define CMD6 0x46	/**< Reserved                                                   */
#define CMD7 0x47	/**< not used in SPI mode                                       */
#define CMD8 0x48	/**< Reserved                                                   */
#define CMD9 0x49	/**< ask card to send card speficic data (CSD)                  */
#define CMD10 0x4A	/**< ask card to send card identification (CID)                 */
#define CMD11 0x4B	/**< not used in SPI mode                                       */
#define CMD12 0x4C	/**< stop transmission on multiple block read                   */
#define CMD13 0x4D	/**< ask the card to send it's status register                  */
#define CMD14 0x4E	/**< Reserved                                                   */
#define CMD15 0x4F	/**< not used in SPI mode                                       */
#define CMD16 0x50	/**< sets the block length used by the memory card              */
#define CMD17 0x51	/**< read single block                                          */
#define CMD18 0x52	/**< read multiple block                                        */
#define CMD19 0x53	/**< Reserved                                                   */
#define CMD20 0x54	/**< not used in SPI mode                                       */
#define CMD21 0x55	/**< Reserved                                                   */
#define CMD22 0x56	/**< Reserved                                                   */
#define CMD23 0x57	/**< Reserved                                                   */
#define CMD24 0x58	/**< writes a single block                                      */
#define CMD25 0x59	/**< writes multiple blocks                                     */
#define CMD26 0x5A	/**< not used in SPI mode                                       */
#define CMD27 0x5B	/**< change the bits in CSD                                     */
#define CMD28 0x5C	/**< sets the write protection bit                              */
#define CMD29 0x5D	/**< clears the write protection bit                            */
#define CMD30 0x5E	/**< checks the write protection bit                            */
#define CMD31 0x5F	/**< Reserved                                                   */
#define CMD32 0x60	/**< Sets the address of the first sector of the erase group	*/ 
#define CMD33 0x61	/**< Sets the address of the last sector of the erase group     */
#define CMD34 0x62	/**< removes a sector from the selected group                   */
#define CMD35 0x63	/**< Sets the address of the first group                        */
#define CMD36 0x64	/**< Sets the address of the last erase group                   */
#define CMD37 0x65	/**< removes a group from the selected section                  */
#define CMD38 0x66	/**< erase all selected groups                                  */
#define CMD39 0x67	/**< not used in SPI mode                                       */
#define CMD40 0x68	/**< not used in SPI mode                                       */
#define CMD41 0x69	/**< Reserved                                                   */
#define CMD42 0x6A	/**< locks a block                                              */
#define CMD55 0x77
// CMD43 ... CMD57 are Reserved
#define CMD58 0x7A	/**< reads the OCR register										*/
#define CMD59 0x7B	/**< turns CRC off												*/
// CMD60 ... CMD63 are not used in SPI mode

/** @cond */
// SPI Response Types
#define R1    0x01
#define R1b   0x02
#define R2    0x03
#define R3    0x04
#define R7    0x05

// SPI Response Flags
#define IN_IDLE_STATE	   	(unsigned char)(0x01)
#define ERASE_RESET	   	(unsigned char)(0x02)
#define ILLEGAL_COMMAND	   	(unsigned char)(0x04)
#define COM_CRC_ERROR	   	(unsigned char)(0x08)
#define ERASE_ERROR	   	(unsigned char)(0x10)
#define ADRESS_ERROR	   	(unsigned char)(0x20)
#define PARAMETER_ERROR	   	(unsigned char)(0x40)


// Card Status Flags
#define STATUS_CARD_LOCKED      0x0001
#define STATUS_WP_SKIP          0x0002
#define STATUS_ERROR            0x0004
#define STATUS_CC_ERROR         0x0008
#define STATUS_ECC_FAILED       0x0010
#define STATUS_WP_VIOLATION     0x0020
#define STATUS_ERASE_PARAM      0x0040
#define STATUS_OUT_OF_RAGE      0x0080
#define STATUS_IN_IDLE_STATE	0x0100
#define STATUS_ERASE_RESET	   	0x0200
#define STATUS_ILLEGAL_COMMAND	0x0400
#define STATUS_COM_CRC_ERROR	0x0800
#define STATUS_ERASE_ERROR	   	0x1000
#define STATUS_ADRESS_ERROR	   	0x2000
#define STATUS_PARAMETER_ERROR	0x4000

// Control Tokens
#define DATA_RESP_TOKEN_MASK     0x1F
#define DATA_RESP_TOKEN_ACCEPT   0x05
#define DATA_RESP_TOKEN_CRC_ERR  0x0B
#define DATA_RESP_TOKEN_WR_ERR   0x0D

#define DATA_BLOCK_START         0xFE
#define DATA_BLOCK_MULTI_START   0xFC
#define DATA_BLOCK_MULTI_STOP    0xFD

#define DATA_ERR_TOKEN_MASK      0x0F
#define DATA_ERR_TOKEN_ERROR     0x01
#define DATA_ERR_TOKEN_CC_ERR    0x02
#define DATA_ERR_TOKEN_ECC_ERR   0x04
#define DATA_ERR_TOKEN_RANGE_ERR 0x08

// Busy types
#define WAIT_READ                0x01
#define WAIT_WRITE               0x02
#define WAIT_ERASE               0x03

#define CARD_RESPONSE_MAX_COUNT 0x7FFF
#define DMA_MMC_MAX_WRITESTATE_COUNT 0xFFFF
/** @endcond */


// data structures for SDCARD
typedef enum{
    UNSUPPORTED_CARD = 0,  /**< EcoBT not support */
    SD,                    /**< SD card */
    SDHC,                  /**< SDHC card */
    SDXC                   /**< SDXC card */
} SD_CardType;

/** @union SD_CSD_V2_t
*   @brief a union of SD Card module
*/
typedef union{
  unsigned char data[16];
  struct SD_CSD_V2{
    unsigned char   resesrved_1:6,
                    CSD_STRUCTURE:2;
    unsigned char   TAAC;
    unsigned char   NSAC;
    unsigned char   TRAN_SPEED;
    unsigned short  READ_BL_LEN:4,
                    CCC:12;
    unsigned long   C_SIZE:22,
                    reserved_2:6,
                    DSR_IMP:1,
                    READ_BLK_MISALIGN:1,
                    WRITE_BLK_MISALIGN:1,
                    READ_BL_PARTIAL:1;
    unsigned short  WP_GRP_SIZE:7,
                    SECT_SIZE:7,
                    ERASE_BLK_EN:1,
                    reserved_3:1;
    unsigned short  reserved_4:5,
                    WRITE_BL_PARTIAL:1,
                    WRITE_BL_LEN:4,
                    R2W_FACTOR:3,
                    reserved_5:2,
                    WP_GRP_ENABLE:1;
    unsigned char   reserved_6:2,
                    FILE_FORMAT:2,
                    TMP_WRITE_PROTECT:1,
                    PERM_WRITE_PROTECT:1,
                    COPY:1,
                    FILE_FORMAT_GRP:1;
    unsigned char   ALWAYS_1:1,
                    CRC:7;
  };
} SD_CSD_V2_t;


/** @cond */
// port define
#define SD_CS        P2_0
#define SD_CLK       P0_4
#define SD_MISO      P0_2
#define SD_MOSI      P0_3
/** @endcond */
// functions

/** @brief SD Card testing
*
*/
void sd_test(void);

/** @brief Setup SPI function for SD Card module
*	@param [in] fast SD Card working speed
*/
void hw_init(uint8 fast);

/** @brief Send command to SD Card module over SPI
*	@param [in] cmd		The command send to SD Card
* 	@param [in] *args	Pointer to the data be wrote to SD Card
* 	@param [in] crc		Send out crc, only needed when CRC is enabled
* 	@param [in] type	SPI Response Types
* 	@param [in] *resp	Pointer to store the data of SPI response 
*/
uint8 sd_cmd(uint8 cmd, uint8* args, uint8 crc, uint8 type, uint8* resp);
uint8 sd_init(void);
uint8 sd_reset(void);
uint8 sd_condChk(void);
uint8 sd_readOCR(uint8 *resp);
uint8 sd_activeInit(uint8 HCS);
uint8 sd_setBlockLen(void);
uint8 sd_readCID(uint8 *ret);
uint8 sd_readCSD(uint8 *ret);
uint8 sd_readStatus(uint8 *ret);
uint8 sd_readRegister(uint8 len, uint8* buf);
uint8 sd_block_cmd(uint8 cmd, uint32 parameter);
uint8 sd_block_read(uint32 blockIdx, uint8 *buf, uint16 len);
uint8 sd_read_single_block(uint32 blockAddr);
uint8 sd_block_data_read(uint8 *buf, uint16 bsize);
uint8 sd_block_write(uint32 blockIdx, uint8 *buf);
uint8 sd_single_block_write(uint32 blockAddr);
uint8 sd_block_data_write(uint8 *buf, uint16 bsize);
uint8 sd_wait_busy(uint8 type);
uint32 sd_get_max_block_num(void);

#ifdef __cplusplus
}
#endif

#endif

/**
* @}
*/