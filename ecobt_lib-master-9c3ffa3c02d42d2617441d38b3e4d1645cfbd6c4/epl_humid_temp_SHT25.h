#ifndef EPL_HUMID_TEMP_SHT25_H
#define EPL_HUMID_TEMP_SHT25_H

#ifdef __cplusplus
extern "C"
{
#endif

#include "hal_types.h"
#include "epl_hal_i2c.h"
#include "hal_mcu.h"

#define SHT25_I2C_ADDR_W                0x80    // 1000 0000
#define SHT25_I2C_ADDR_R                0x81    // 1000 0001
#define SHT25_T_MEASURE_HOLD_MASTER     0xE3    // 1110 0011
#define SHT25_T_MEASURE_NO_HOLD_MASTER  0xF3    // 1110 0101
#define SHT25_RH_MEASURE_HOLD_MASTER    0xE5    // 1111 0011
#define SHT25_RH_MEASURE_NO_HOLD_MASTER 0xF5    // 1111 0101
#define SHT25_WRITE_USER_REGISTER       0xE6    // 1110 0110
#define SHT25_READ_USER_REGISTER        0xE7    // 1110 0111
#define SHT25_SOFT_RESET                0xFE    // 1111 1110

//***********************************************************************************
// Macros

// Wait 1+1/3*t [us]
#define WAIT_1_3US(t)                   \
    do{                                 \
        for (uint8 i = 0; i<t; i++)     \
            asm("NOP");                 \
    }while(0)

//***********************************************************************************
// Function prototypes
unsigned char sht25Init(void);
void sht25StartTemp(void);
void sht25StartHumid(void);
unsigned char sht25Read(unsigned short *reading, unsigned char *chk);
void sht25GetTemp(uint8 *temp_h, uint8 *temp_l, uint8 *chk);
void sht25GetHumid(uint8 *humid_h, uint8 *humid_l, uint8 *chk);
void sht25SoftReset(void);

#ifdef __cplusplus
}
#endif

#endif
