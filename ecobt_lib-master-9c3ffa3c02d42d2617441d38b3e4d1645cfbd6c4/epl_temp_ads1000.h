 /**
 * 
 * @ingroup Module
 * @defgroup ADC
 * @{
 * @file epl_temp_ads1000.h
 * 
 *
 * @author 
 * @date 2013/5/10
 * @copyright 
 * Copyright (c) 2013 EPLAB National Tsing Hua University . All rights reserved.\n
 * The information contained herein is confidential property of NTHU. 
 * The material may be used for a personal and non-commercial use only in connection with 
 * a legitimate academic research purpose.  
 * Any attempt to copy, modify, and distribute any portion of this source code or derivative thereof for commercial, 
 * political, or propaganda purposes is strictly prohibited.  
 * All other uses require explicit written permission from the authors and copyright holders. 
 * This copyright statement must be retained in its entirety and displayed 
 * in the copyright statement of derived source code or systems.
 */


#ifndef EPL_TEMP_ADS1000_H
#define EPL_TEMP_ADS1000_H

#ifdef __cplusplus
extern "C"
{
#endif

#include "hal_types.h"
#include "epl_hal_i2c.h"
#include "hal_mcu.h"
#include "hal_led.h"

/** @cond */
#define ADS1000_I2C_ADDR_W                0x90    // 1001 0000
#define ADS1000_I2C_ADDR_R                0x91    // 1001 0000
/** @endcond */


//***********************************************************************************
// Macros

// Wait 1+1/3*t [us]
/** @def WAIT_1_3US(t)
*		Delay for t CPU circle
*/
#define WAIT_1_3US(t)                   \
    do{                                 \
        for (uint8 i = 0; i<t; i++)     \
            asm("NOP");                 \
    }while(0)
	

//***********************************************************************************
// Function prototypes

/** @brief Initial ADS
*/ 
void ads1000Init(void);

/** @brief Read temp from adc
*	@param[out] temp 
*		Pointer to store the data read from ADC
*/ 
void ads1000GetTemp(uint8 *temp);

/** @brief Write data to specific address
*	@param[in] conf 
*		Pointer to store the data to write to ADC
*/ 
void ads1000SetReg(uint8 conf);

/** @brief Soft reset the ADC
*/ 
void ads1000SoftReset();

#ifdef __cplusplus
}
#endif

#endif

/**
@}
*/
