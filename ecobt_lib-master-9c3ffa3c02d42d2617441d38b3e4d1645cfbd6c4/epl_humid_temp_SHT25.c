#include <ioCC2540.h>
#include "hal_types.h"
#include "epl_hal_i2c.h"
#include "epl_humid_temp_SHT25.h"

unsigned char sht25Init(void){
  uint8 i, ack;
  // detecting if sht25 is connected
  ack = 0;
  for (i = 0; i < 4; i++)
  {
    I2CStart();
    ack |= I2CSentByte( SHT25_I2C_ADDR_W );
    I2CStop();
    WAIT_1_3US(160);
  }
  if ( ack == 0 ){
    return TRUE;
  }else{
    return FALSE;
  }
}

void sht25StartTemp(void){
  I2CStart();
  while( I2CSentByte( SHT25_I2C_ADDR_W ) != 0 );
  while( I2CSentByte( SHT25_T_MEASURE_NO_HOLD_MASTER ) != 0 );
}

void sht25StartHumid(void){
  I2CStart();
  while( I2CSentByte( SHT25_I2C_ADDR_W ) != 0 );
  while( I2CSentByte( SHT25_RH_MEASURE_NO_HOLD_MASTER ) != 0 );
}

unsigned char sht25Read(unsigned short *reading, unsigned char *chk){
  uint8 ack;
  uint8 data_h, data_l;
  I2CStart();
  ack = I2CSentByte( SHT25_I2C_ADDR_R );
  if (ack == 0){
    // reading is ready
    data_h = I2CReceiveByte(LOW);
    data_l = I2CReceiveByte(LOW);
    *chk = I2CReceiveByte(HIGH);
    *reading = (data_h) * 256 + data_l;
  }else{
    // reading is not ready
  }
  I2CStop();
  if (ack == 0){
    return 1;   // read done
  }else{
    return 0;   // not ready
  }
}

void sht25GetTemp(uint8 *temp_h, uint8 *temp_l, uint8 *chk){
  uint16 i;
  uint8 ack;
  I2CStart();
  while( I2CSentByte( SHT25_I2C_ADDR_W ) != 0 );
  while( I2CSentByte( SHT25_T_MEASURE_NO_HOLD_MASTER ) != 0 );
  I2CStop();	
  // wait for reading ready
  while (1){
    for (i = 0; i < 100; i++){
      WAIT_1_3US(160);
      WAIT_1_3US(160);
    }
    I2CStart();
    ack = I2CSentByte( SHT25_I2C_ADDR_R );
    if ( ack == 0)
      break;
  }
  *temp_h = I2CReceiveByte(LOW);
  *temp_l = I2CReceiveByte(LOW);
  *chk = I2CReceiveByte(HIGH);
  I2CStop();	
}

void sht25GetHumid(uint8 *humid_h, uint8 *humid_l, uint8 *chk){
  uint16 i;
  uint8 ack;
  I2CStart();
  while( I2CSentByte( SHT25_I2C_ADDR_W ) != 0 );
  while( I2CSentByte( SHT25_RH_MEASURE_NO_HOLD_MASTER ) != 0 );
  I2CStop();	
  // wait for reading ready
  while (1){
    for (i = 0; i < 100; i++){
      WAIT_1_3US(160);
      WAIT_1_3US(160);
    }
    I2CStart();
    ack = I2CSentByte( SHT25_I2C_ADDR_R );
    if ( ack == 0)
      break;
  }
  *humid_h = I2CReceiveByte(LOW);
  *humid_l = I2CReceiveByte(LOW);
  *chk = I2CReceiveByte(HIGH);
  I2CStop();	
}

void sht25SoftReset(void){
  I2CStart();
  while( I2CSentByte( SHT25_I2C_ADDR_W ) != 0 );
  while( I2CSentByte( SHT25_SOFT_RESET ) != 0 );
  I2CStop();
}

