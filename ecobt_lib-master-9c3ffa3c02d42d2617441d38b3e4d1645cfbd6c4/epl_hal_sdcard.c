#include <ioCC2540.h>
#include "epl_hal_sdcard.h"
#include "epl_hal_spi.h"

#include "epl_led.h"
#include "hal_led.h"

#define spiWriteByte  spi0WriteByte
#define spiReadByte   spi0ReadByte

#define SPI_DUMMY     0xFF


static SD_CSD_V2_t XDATA csd;

void sd_test(void)
{
  uint16 i;
  uint8 buf[512];
//  sd_block_read(0, sd_buf1);
//  printf("Block 0\n");
//  for (i = 0; i < 15; i++)
//  {
//    printf("%02X: 0x%02X\n", i, sd_buf1[i]);
//  }
//  for (i = 0; i < 10; i++)
//  {
//    buf[i] = 'A' + i;
//  }
//  sd_block_write(1, buf);
  for (i = 0; i < 512; i++)
  {
    buf[i] = 0xFF;
  }
  sd_block_read(1, buf, 512);
  sd_block_read(2, buf, 512);
  for (i = 0; i < 512; i++)
  {
    buf[i] = i % 256;
  }
//  printf("Block 0\n");
//  for (i = 0; i < 15; i++)
//  {
//    printf("%02X: 0x%02X\n", i, buf[i]);
//  }
}


void hw_init(uint8 fast){
    // init SPI0
  // USART 0 at default location 0
  PERCFG &= ~0x01;
  // SPI0
  P0SEL |= 0x2D;

  U0CSR &= ~0xA0;
  // MSB first
  U0GCR = 0x20;
  if (fast)
  {
    // SCK frequency = 4MHz, low speed for setup
    U0GCR |= 0x11;
    U0BAUD = 0x00;
  }
  else
  {
    // SCK frequency = 250kHz, low speed for setup
    U0GCR |= 0x0D;
    U0BAUD = 0x00;
  }
  // SD CS Setup
  P2DIR |= 0x01;
}

void SD_EnableCS(void)
{
  SD_CS = 0;
}

void SD_DisableCS(void)
{
  SD_CS = 1;
}

uint8 sd_cmd(uint8 cmd, uint8* args, uint8 crc, uint8 type, uint8* resp)
{
  uint8 len;
  uint8 temp;
  uint16 count;
  SD_EnableCS();
  // wait for card ready
//  do
//  {
//    spiReadByte(&temp, SPI_DUMMY);
//  }
//  while(temp == 0xFF);

  // send out command
  spiWriteByte(cmd);
  // send out parampeters
  for (count = 0; count < 4; count++)
  {
    spiWriteByte(args[count]);
  }
  // send out crc, only needed when CRC is enabled
  spiWriteByte(crc);

  // get response
  len = 0;
  switch(type)
  {
  case R1:
  case R1b:
    len = 1;
    break;
  case R2:
    len = 2;
    break;
  case R3:
  case R7:
    len = 5;
    break;
  default:
    //  should not go to here
    SD_DisableCS();
    return 1;
    break;
  }

  // Wait for valid response
  for (count = 0; count < 4096; count++)
  {
    spiReadByte(&temp, SPI_DUMMY);
    if (!(temp & 0x80))
    {
      *(resp++) = temp;
      break;
    }
  }
  if (count >= 4096)
  {
    // command timeout
    spiWriteByte(SPI_DUMMY);
    return 1;
  }
  // Read response
  for (count = 1; count < len; count++)
  {
    spiReadByte(&(*(resp++)), SPI_DUMMY);
  }

  SD_DisableCS();
  return 0;
}

uint8 sd_init(void)
{
  uint8 resp;
  uint16 count;
  uint8 ret[5];
  uint8 param[4] = {0x00, 0x00, 0x00, 0x00};
  uint8 crc = 0x95;

  // setup spi in slow mode
  hw_init(0);
  // Make sd card enter SPI mode
  SD_DisableCS();
  for (count = 0; count < 10; count++)
  {
    spiWriteByte(0xFF);
  }

  // GO_IDLE_STATE, CMD0
  resp = sd_reset();
  if (resp != 0)
  {
    return resp;
  }
  epl_ledGreen_off();

  // SEND_IF_COND, CMD8
  resp = sd_condChk();
  if (resp == 0xFF)
  {
    return 0xFF;
  }

  if (resp != 0)
  {
    // SD Card V1.x
    // read OCR
    resp =  sd_readOCR(ret);
//    printf("OCR reg:\n");
//    for (count = 0; count < R3; count++)
//      printf("0x%0X\n", ret[count]);
    if (resp != 0)
    {
      return resp;
    }

    resp = sd_cmd(CMD59, param, crc, R1, ret);
//    printf("CMD59: 0x%0X\n", ret[0]);
    // finish init. process
    resp = sd_activeInit(0);
//    printf("init: 0x%0X\n", ret[0]);
    if (resp != 0)
    {
      return resp;
    }
    // SD Card V1.x init. completed
  }
  else
  {
    // SD Card V2.x
    // read OCR
    resp = sd_readOCR(ret);
    if (resp != 0)
    {
      return resp;
    }
//    printf("OCR reg:\n");
//    for (count = 0; count < R3; count++)
//      printf("0x%02X\n", ret[count]);
    // finish init. process
    resp = sd_activeInit(1);
    if (resp != 0)
    {
      return resp;
    }
    // get CCS
    resp = sd_readOCR(ret);
    if (resp != 0)
    {
      return resp;
    }
//    printf("OCR reg:\n");
//    for (count = 0; count < R3; count++)
//      printf("0x%02X\n", ret[count]);
    // SD Card V2.x init. completed
  }

  // back to high speed mode
  hw_init(1);

  // set block length
  sd_setBlockLen();

  HAL_LED_BLINK_DELAY();

  //read CID
//  sd_readCID(ret);
//  printf("CID reg:\n");
//  printf("0x%02X\n", ret[0]);
//  printf("0x%02X  ", ret[1]);
//  printf("0x%02X\n", ret[2]);
//  printf("%c", ret[3]);
//  printf("%c", ret[4]);
//  printf("%c", ret[5]);
//  printf("%c", ret[6]);
//  printf("%c\n", ret[7]);
//  printf("V%02X\n", ret[8]);


  //read CSD
  sd_readCSD(csd.data);
//  printf("CSD reg:\n");
//  for (count = 0; count < 18; count++)
//  {
//    printf("0x%02X\n", csd.data[count]);
//  }

//  uint8 buf[40];
//  sd_block_read(0, buf, 40);
//  printf("Block 0\n");
//  for (uint8 cnt = 0; cnt < 40; cnt++)
//  {
//  printf("%02X: 0x%02X %c\n", cnt, buf[cnt], buf[cnt]);
//  }


  return 0;
}

uint8 sd_reset(void)
{
  uint8 param[4] = {0x00, 0x00, 0x00, 0x00};
  uint8 crc = 0x95;
  uint8 err, resp;

  err = sd_cmd(CMD0, param, crc, R1, &resp);
  if (!err)
  {
    if (resp == 0x01)
    {
      return 0;
    }
    return resp;
  }
  else
  {
    return err;
  }
}

uint8 sd_condChk(void)
{
  uint8 param[4] = {0x00, 0x00, 0x01, 0xAA};
  uint8 crc = 0x87;
  uint8 resp[R7];
  uint8 err;

  err = sd_cmd(CMD8, param, crc, R7, resp);

  if (!err)
  {
    if (resp[0] & ILLEGAL_COMMAND)
    {
      return resp[0];
    }
    else
    {
      if (resp[3] != 0x01)
      {
        return 0xFF;
      }
      if (resp[4] != 0xAA)
      {
        return 0xFF;
      }
      return 0;
    }
  }
  else
  {
    return 0xFF;
  }
}

uint8 sd_readOCR(uint8 *ret)
{
  uint8 param[4] = {0x00, 0x00, 0x00, 0x00};
  uint8 crc = 0x95;
  uint8 resp[R7];
  uint8 err;
  uint8 i;

  err = sd_cmd(CMD58, param, crc, R3, resp);

  if (err != 0)
  {
    return err;
  }

  for (i = 0 ; i < R7; i++){
    ret[i] = resp[i];
  }

  return err;
}

uint8 sd_activeInit(uint8 HCS)
{
  uint8 param[4] = {0x00, 0x00, 0x00, 0x00};
  uint8 crc = 0x95;
  uint8 resp55, resp41;
  uint8 err;
  uint16 count;

  // wait until the SD card initialize complete
  count = 0;
  do
  {
    // the next command is an application specific command
    param[0] = 0x00;
    err = sd_cmd(CMD55, param, crc, R1, &resp55);
//    printf("CMD55: 0x%02X\n", resp55);
    if (err != 0)
    {
      return err;
    }
    if (HCS)
    {
      param[0] = 0x40;
    }
    err = sd_cmd(CMD41, param, crc, R1, &resp41);
//    printf("CMD41: 0x%02X\n", resp41);
    if (err != 0)
    {
      return err;
    }
    count++;
    if (resp41 == 0x00)
    {
      break;
    }
  } while(count < 4096);

  if (count >= 4096)
  {
    return 0xFF;
  }
  return 0;
}

uint8 sd_setBlockLen(void)
{
  // set block len to 512
  uint8 param[4] = {0x00, 0x00, 0x02, 0x00};
  uint8 crc = 0xFF;
  uint8 resp;
  uint8 err;

  err = sd_cmd(CMD16, param, crc, R1, &resp);
  if (err != 0)
  {
    return err;
  }

  return 0;
}

uint8 sd_readCID(uint8 *ret)
{
  uint8 param[4] = {0x00, 0x00, 0x00, 0x00};
  uint8 crc = 0x95;
  uint8 resp;
  uint8 err;

  err = sd_cmd(CMD10, param, crc, R1, &resp);
  if (err != 0)
  {
    return err;
  }

  return sd_readRegister(16, ret);
}

uint8 sd_readCSD(uint8 *ret)
{
  uint8 param[4] = {0x00, 0x00, 0x00, 0x00};
  uint8 crc = 0x95;
  uint8 resp;
  uint8 err;
  uint8 temp;

  err = sd_cmd(CMD9, param, crc, R1, &resp);
  if (err != 0)
  {
    return err;
  }
  if (resp != 0)
  {
    return resp;
  }

  err = sd_readRegister(16, ret);
  temp = ret[4];
  ret[4] = ret[5];
  ret[5] = temp;
  temp = ret[6];
  ret[6] = ret[9];
  ret[9] = temp;
  temp = ret[7];
  ret[7] = ret[8];
  ret[8] = temp;
  temp = ret[10];
  ret[10] = ret[11];
  ret[11] = temp;
  temp = ret[12];
  ret[12] = ret[13];
  ret[13] = temp;

  return err;
}

uint8 sd_readStatus(uint8 *ret)
{
  uint8 param[4] = {0x00, 0x00, 0x00, 0x00};
  uint8 crc = 0x95;
  uint8 err;

  err = sd_cmd(CMD13, param, crc, R2, ret);
  if (err != 0)
  {
    return err;
  }

  return 0;
}

uint8 sd_readRegister(uint8 len, uint8* buf)
{
  uint16 i;
  uint8 tmp;

  SD_EnableCS();
  // wait for data start token
  i = 0;
  do
  {
    spiReadByte(&tmp, SPI_DUMMY);
    if (tmp != SPI_DUMMY)
    {
      break;
    }
  } while(i < 4096);
  if (i >= 4096)
  {
    spiWriteByte(SPI_DUMMY);
    SD_DisableCS();
    return 0xFF;
  }
  // read data block
  if (tmp != DATA_BLOCK_START)
  {
    i = 1;
  }
  else
  {
    i = 0;
  }
  // read data block
  for (; i < len; i++){
    spiReadByte(&buf[i], SPI_DUMMY);
  }
  // read CRC16
  spiReadByte(&buf[i++], SPI_DUMMY);
  spiReadByte(&buf[i], SPI_DUMMY);
  // calculate CRC?

  spiWriteByte(SPI_DUMMY);
  SD_DisableCS();
  return 0;
}

uint8 sd_block_cmd(uint8 cmd, uint32 parameter)
{
  uint8 param[4] = {0x00, 0x00, 0x00, 0x00};
  uint8 crc = 0x95;
  uint8 resp;
  uint8 err;

  param[0] = (uint8)(parameter >>24);
  param[1] = (uint8)(parameter >>16);
  param[2] = (uint8)(parameter >> 8);
  param[3] = (uint8)(parameter);

  err = sd_cmd(cmd, param, crc, R1, &resp);
  if (err != 0)
  {
    return err;
  }
  if (resp != 0)
  {
    return resp;
  }
  return 0;
}

// SD Card R/W/Erase operations
uint8 sd_block_read(uint32 blockIdx, uint8 *buf, uint16 len)
{
  uint8 err;
  // check range
  // set read address
  err = sd_read_single_block(blockIdx);
  if (err != 0)
  {
    return err;
  }
  // read data
  if (len > 512)
  {
    len = 512;
  }
  err = sd_block_data_read(buf, len);

  return err;
}

uint8 sd_read_single_block(uint32 blockAddr)
{
  return sd_block_cmd(CMD17, blockAddr);
}

uint8 sd_block_data_read(uint8 *buf, uint16 bsize)
{
  uint8 tmp;
  uint32 i = 0;

  SD_EnableCS();
  // wait for data start token
  i = 0;
  do
  {
    spiReadByte(&tmp, SPI_DUMMY);
    if (tmp != SPI_DUMMY)
    {
      break;
    }
  } while(i < 4096);
  if (i >= 4096)
  {
    spiWriteByte(SPI_DUMMY);
    SD_DisableCS();
    return 0xFF;
  }
  // read data
  if (tmp != DATA_BLOCK_START)
  {
    spiWriteByte(SPI_DUMMY);
    SD_DisableCS();
    return tmp;
  }

  for (i = 0; i < bsize; i++)
  {
    spiReadByte(&buf[i], SPI_DUMMY);
  }
  // CRC16
  spiWriteByte(SPI_DUMMY);
  spiWriteByte(SPI_DUMMY);

  // End
  spiWriteByte(SPI_DUMMY);
  SD_DisableCS();
  return 0;
}

uint8 sd_block_write(uint32 blockIdx, uint8 *buf)
{
  uint8 err;
  uint8 status[2];
  // check range
//  if (blockIdx >= )
  // set write address
  err = sd_single_block_write(blockIdx);
  if (err != 0)
  {
    return err;
  }
  // write data
  err = sd_block_data_write(buf, 512);
  if (err == 0)
  {
    // check SD Card status
    err = sd_readStatus(status);
    if (err != 0)
    {
      return err;
    }
    if ((status[1] != 0) || (status[0] != 0))
    {
      return 0xFF;
    }
  }
  return err;
}

uint8 sd_single_block_write(uint32 blockAddr)
{
  return sd_block_cmd(CMD24, blockAddr);
}

uint8 sd_block_data_write(uint8 *buf, uint16 bsize)
{
  uint32 i;
  uint8 resp;

  SD_EnableCS();
  // send block write start token
  spiWriteByte(DATA_BLOCK_START);

  for (i = 0; i < bsize; i++)
  {
    spiWriteByte(buf[i]);
  }
  // CRC16, send dummy bytes
  spiWriteByte(SPI_DUMMY);
  spiWriteByte(SPI_DUMMY);

  // read data responce
  spiReadByte(&resp, SPI_DUMMY);
  if ((resp & DATA_RESP_TOKEN_MASK) != DATA_RESP_TOKEN_ACCEPT)
  {
    // data write failed
    spiWriteByte(SPI_DUMMY);
    SD_DisableCS();
    return resp;
  }

  // End
  SD_DisableCS();
  //
  if(sd_wait_busy(WAIT_WRITE) != 0)
  {
    return 0xFF;
  }
  else
  {
    return 0;
  }
}

uint8 sd_wait_busy(uint8 type)
{
  uint32 count, i;
  uint8 resp;
  if (type == WAIT_READ)
  {
    count = 4096;
  }
  else if(type == WAIT_WRITE)
  {
    count = 4096;
  }
  else // WAIT_ERASE
  {
    count = 4096;
  }

  SD_EnableCS();
  i = 0;
  do
  {
    spiReadByte(&resp, SPI_DUMMY);
    i++;
  } while((resp != 0xFF) && (i < count));
  SD_DisableCS();

  if (i >= count)
  {
    return 0xFF;
  }
  return 0;
}

uint32 sd_get_max_block_num(void)
{
  return (csd.C_SIZE + 1);
}