#include <ioCC2540.h>
#include "hal_types.h"
#include "epl_hal_i2c.h"
#include "epl_led.h"
#include "epl_temp_ads1000.h"

void ads1000Read(uint8 *out_h, uint8 *out_l, uint8 *conf){
  I2CStart();
  while( I2CSentByte( ADS1000_I2C_ADDR_R ) != 0 );
  *out_h = I2CReceiveByte(LOW);
  *out_l = I2CReceiveByte(LOW);
  *conf = I2CReceiveByte(LOW);
  I2CStop();  
}

void ads1000Write(uint8 conf){
  I2CStart();
  while( I2CSentByte( ADS1000_I2C_ADDR_W ) != 0 );
  while( I2CSentByte( conf ) != 0 );
  I2CStop();  
}

void ads1000Init(void);

void ads1000GetTemp(uint8 *temp){
  uint8 i, count;
  uint8 out_h, out_l, conf;
  ads1000SoftReset();
  ads1000Write(0x90);
  count = 0;
  while(1){
    count++;
    for (i = 0; i < 25; i++)
      WAIT_1_3US(160);
    ads1000Read(&out_h, &out_l, &conf);
    if (conf == 0x10)
      break;
  }
  *temp = count;
}

void ads1000SoftReset(){
  I2CStart();
  while( I2CSentByte( 0x00 ) != 0 );
  while( I2CSentByte( 0x06 ) != 0 );
  I2CStop();
}
