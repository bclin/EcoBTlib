/*
 * ADS1298.c
 *
 *  Created on: 2011/2/25
 *      Author: GuoHuei
 */

#include <ioCC2540.h>
#include "epl_ecg_ads1298.h"


void (*callback)(uint8 *data);

#define spiWriteByte  spi1WriteByte
#define spiReadByte   spi1ReadByte


#define delay_ADS1298_SEND_COMMAND() 	\
  do {	        \
    __asm	\
      NOP	\
      NOP	\
      NOP	\
      NOP	\
      NOP	\
      NOP	\
      NOP	\
      NOP	\
    __endasm;	\
  }while(0);

#define WAIT_1_3US(t)                   \
    do{                                 \
        for (uint16 i = 0; i<t; i++)     \
            asm("NOP");                 \
    }while(0)


#define CS_DISABLED     1
#define CS_ENABLED      0

ADS1298_RegMap_t ads1298_regmap;

void ADS1298_SEND_COMMAND(ADS1298_OPCODE opcode)
{
  ADS1298_CS = CS_ENABLED;
  spiWriteByte(opcode);
  ADS1298_CS = CS_DISABLED;
//  delay_ADS1298_SEND_COMMAND();
}

void ADS1298_WREG(unsigned char addr, unsigned char * payload, unsigned char len)
{
  unsigned char i;
  ADS1298_CS = CS_ENABLED;
  spiWriteByte(ADS129X_WREG | addr);
  spiWriteByte(len - 1);
  for (i = 0; i < len; i++){
    spiWriteByte(payload[i]);
//    hal_spi_master_read_write(payload[i]);
  }
  ADS1298_CS = CS_DISABLED;
}

void ADS1298_RREG(unsigned char addr, unsigned char * payload, unsigned char len)
{
  unsigned char i;
  ADS1298_CS = CS_ENABLED;
  spiWriteByte(ADS129X_RREG | addr);
  spiWriteByte(len - 1);
//  hal_spi_master_read_write(ADS129X_RREG | addr);
//  hal_spi_master_read_write(len - 1);
  for (i = 0; i < len; i++){
    spiReadByte(&payload[i], 0x00);
//    payload[i] = hal_spi_master_read_write(0x00);
  }
  ADS1298_CS = CS_DISABLED;
}

void ADS1298_init()
{
  // Setup SPI
  // USART 1 at alternate location 2
  PERCFG |= 0x02;
  // Peripheral function on SCK, MISO and MOSI (P1_5-7)
  P1SEL |= 0xE0;

  //*** Setup the SPI interface ***
  // SPI Master Mode
  U1CSR &= ~0xA0;
  // Negative clock polarity, Phase: data out on CPOL -> CPOL-inv
  //                                 data in on CPOL-inv -> CPOL
  // MSB first
  U1GCR = 0x60; // CPOL = 0, CPHA = 1
  // SCK frequency = 4MHz
  U1GCR |= 0x11;
  // SCK frequency = 1MHz
//  U1GCR |= 0x0F;
  U1BAUD = 0x00;

  // set CS dir
  P1SEL &= ~(1 << 4);
  P1DIR |= 1 << 4;
  // set up START, DRDY dir
  P2DIR &= ~(1 << 1);
  P2DIR |= 1 << 2;

  // Enable DRDY Interrupt
  PICTL |= 0x08;    // port interrupt control, 0 rising edge, 1 falling edge
  P2IEN |= 0x02;    // IOC module, prot 0 interrupt mask, 0 interrupts are disabled, 1 interrupt s are enabled.
  IEN2  |= 0x02;       // CPU module, prot 0 interrupt mask, 0 interrupts are disabled, 1 interrupt s are enabled.
  P2IFG  &= ~(0x02);    // P2 interrupt Flag Status
  P2IF   = 0;       // port 2 interrupt status flag
  EA     = 1;       // enable interrupt

  ADS1298_START = 1;
  ADS1298_CS = CS_DISABLED;
}
/*
void ADS1298_set_FunctionCall(void (*p)(void))
{
	functionCall = p;
	if (functionCall){
		EX0 = 1;
	}else{
		EX0 = 0;
	}
}

void ADS1298_DRDY_ISR(void) interrupt INTERRUPT_IPF
{
	if (functionCall)
		(*functionCall)();
}
*/

void ECG_init(void)
{
  // Default ADS1298 configuration
  ads1298_regmap.config.RegMap.ID          = 0x00;
  // DR[2:0] conf sampling rate, 110 as 250sps, 101 as 500sps, 100 as 1ksps
  ads1298_regmap.config.RegMap.CONFIG1     = 0x06;
//  regmap.config.RegMap.CONFIG1     = 0x04;
  ads1298_regmap.config.RegMap.CONFIG2     = 0x10;
  ads1298_regmap.config.RegMap.CONFIG3     = 0xDC;
  ads1298_regmap.config.RegMap.LOFF        = 0x03;
  ads1298_regmap.config.RegMap.CH1SET      = 0x00;
  ads1298_regmap.config.RegMap.CH2SET      = 0x00;
  ads1298_regmap.config.RegMap.CH3SET      = 0x00;
  ads1298_regmap.config.RegMap.CH4SET      = 0x00;
  ads1298_regmap.config.RegMap.CH5SET      = 0x00;
  ads1298_regmap.config.RegMap.CH6SET      = 0x00;
  ads1298_regmap.config.RegMap.CH7SET      = 0x00;
  ads1298_regmap.config.RegMap.CH8SET      = 0x00;

  ads1298_regmap.config.RegMap.RLD_SENSP   = 0x00;
  ads1298_regmap.config.RegMap.RLD_SENSN   = 0x00;
  ads1298_regmap.config.RegMap.LOFF_SENSP  = 0xFF;
  ads1298_regmap.config.RegMap.LOFF_SENSN  = 0x06;
  ads1298_regmap.config.RegMap.LOFF_FLIP   = 0x00;
  ads1298_regmap.config.RegMap.LOFF_STATP  = 0x00;
  ads1298_regmap.config.RegMap.LOFF_STATN  = 0x00;
  ads1298_regmap.config.RegMap.GPIO        = 0x00;
  ads1298_regmap.config.RegMap.PACE        = 0x00;
  ads1298_regmap.config.RegMap.RESP        = 0x00;
  ads1298_regmap.config.RegMap.CONFIG4     = 0x02;
  ads1298_regmap.config.RegMap.WCT1        = 0x0B;
  ads1298_regmap.config.RegMap.WCT2        = 0xD4;

  ADS1298_init();

  ECG_reset();
  ECG_stop();
  ECG_load();
}

void ECG_reset(void)
{
  uint8 i;
  ADS1298_START = 0;
  ADS1298_SEND_COMMAND(ADS129X_RESET);
  for (i = 0; i < 5; i++){
    WAIT_1_3US(333);
  }
}

void ECG_start(void)
{
  uint8 i;
  ADS1298_SEND_COMMAND(ADS129X_RDATAC);
  for (i = 0; i < 5; i++){
    WAIT_1_3US(333);
  }
  // start ADS1298
  ADS1298_START = 1;
}

void ECG_stop(void)
{
  uint8 i;
  ADS1298_START = 0;
  ADS1298_SEND_COMMAND(ADS129X_STOP);
  for (i = 0; i < 5; i++){
    WAIT_1_3US(333);
  }
  ADS1298_SEND_COMMAND(ADS129X_SDATAC);
  for (i = 0; i < 5; i++){
    WAIT_1_3US(333);
  }
}

void ECG_getConfig(uint8 *config1, uint8 *config2)
{
  uint8 i;
  for (i = 0; i < 13; i++)
  {
    config1[i] = ads1298_regmap.config.data[i];
    config2[i] = ads1298_regmap.config.data[13 + i];
  }
}

void ECG_setConfig(uint8 *config1, uint8 *config2)
{
  uint8 i;
  for (i = 0; i < 13; i++)
  {
    ads1298_regmap.config.data[i] = config1[i];
    ads1298_regmap.config.data[13 + i] = config2[i];
  }
}

void ECG_load(void)
{
  uint8 i;
//  ADS1298_WREG(0, regmap.config.data, 0x1a);
  ADS1298_WREG(0, ads1298_regmap.config.data, 26);
  for (i = 0; i < 5; i++){
    WAIT_1_3US(333);
  }
}

void ADS1298_set_Callback(void (*p)(uint8 *data))
{
  callback = p;
}

void halProcessECGInterrupt( uint8 ECGEvent )
{
  uint8 i;
  uint8 data[27];
  ADS1298_CS = CS_ENABLED;
  for (i = 0; i < ECG_OUTPUT_LEN; i++)
  {
    spiReadByte(&data[i], 0xFF);
  }
  ADS1298_CS = CS_DISABLED;

  if (callback)
  {
    (*callback)(data);
  }
}

HAL_ISR_FUNCTION( halADS1298Port2Isr, P2INT_VECTOR )
{
  if ( P2IFG & HAL_ADS1298_DRDY_BIT )
  {
    halProcessECGInterrupt(1);
  }

  /*
    Clear the CPU interrupt flag for Port_2
    PxIFG has to be cleared before PxIF
  */
  P2IFG  &= ~(0x02);;
  P2IF = 0;
}
