#ifndef EPL_POTENTIOMETER_AD5170_H
#define EPL_POTENTIOMETER_AD5170_H

#ifdef __cplusplus
extern "C"
{
#endif

#include "hal_types.h"
#include "epl_hal_i2c.h"
#include "hal_mcu.h"

#define AD5170_I2C_ADDR_MASK             0x06    // 0000 0110
#define AD5170_I2C_ADDR_W                0x58    // 0101 1xx0
#define AD5170_I2C_ADDR_R                0x59    // 0101 1xx1

//***********************************************************************************
// Macros

// Wait 1+1/3*t [us]
#define WAIT_1_3US(t)                   \
    do{                                 \
        for (uint8 i = 0; i<t; i++)     \
            asm("NOP");                 \
    }while(0)



//***********************************************************************************
// Function prototypes
unsigned char ad5170Init(unsigned char addr);
void ad5170_test(void);
void ad5170_set_level(unsigned char addr, unsigned char level);
unsigned char ad5170_get_level(unsigned char addr, unsigned char *level);

#ifdef __cplusplus
}
#endif

#endif
