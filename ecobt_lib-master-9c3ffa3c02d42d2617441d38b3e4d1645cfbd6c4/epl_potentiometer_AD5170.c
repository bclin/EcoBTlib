#include <ioCC2540.h>
#include "hal_types.h"
#include "epl_hal_i2c.h"
#include "epl_potentiometer_AD5170.h"
#include "hal_board_cfg.h"
#include "epl_led.h"

void ad5170_test(void)
{
  uint8 test;
  test = ad5170Init(0);
  test = ad5170Init(1);
  test = ad5170Init(2);
  ad5170_set_level(0, 0x03);

  epl_led_init();

  P0SEL &= ~(0x0C);
  P0DIR |= 0x0C;

  while(1)
  {
    epl_ledGreen_on();
//    P0 |= 0x08;        // left hand side green Laser LED
//    P0 |= 0x04;
//    P0 |= (1 << 4);
    P0 &= ~(1 << 2);
    P0 |= (1 << 3);
    HAL_LED_BLINK_DELAY();
    HAL_LED_BLINK_DELAY();
    HAL_LED_BLINK_DELAY();
    HAL_LED_BLINK_DELAY();
    HAL_LED_BLINK_DELAY();
    HAL_LED_BLINK_DELAY();
    HAL_LED_BLINK_DELAY();
    HAL_LED_BLINK_DELAY();
    HAL_LED_BLINK_DELAY();
    HAL_LED_BLINK_DELAY();

    epl_ledGreen_off();
//    P0 &= ~(0x08);
//    P0 &= ~(0x04);
//    P0 &= ~(1 << 4);
    P0 |= (1 << 2);
    P0 &= ~(1 << 3);
    HAL_LED_BLINK_DELAY();
    HAL_LED_BLINK_DELAY();
    HAL_LED_BLINK_DELAY();
    HAL_LED_BLINK_DELAY();
    HAL_LED_BLINK_DELAY();
    HAL_LED_BLINK_DELAY();
    HAL_LED_BLINK_DELAY();
    HAL_LED_BLINK_DELAY();
    HAL_LED_BLINK_DELAY();
    HAL_LED_BLINK_DELAY();
  }
}

unsigned char ad5170Init(unsigned char addr)
{
  uint8 i, ack, salveAddr;
  ack = 0x00;
  salveAddr = AD5170_I2C_ADDR_W | (AD5170_I2C_ADDR_MASK & (addr << 1));
  for (i = 0 ; i < 5; i++)
  {
    I2CStart();
    ack |= I2CSentByte(salveAddr);
    I2CStop();
    WAIT_1_3US(160);
  }

  if (ack == 0)
  {
    return TRUE;
  }
  else
  {
    return FALSE;
  }
}

void ad5170_set_level(unsigned char addr, unsigned char level)
{
  uint8 slaveAddr;
  slaveAddr = AD5170_I2C_ADDR_W | (AD5170_I2C_ADDR_MASK & (addr << 1));

  I2CStart();
  while( I2CSentByte( slaveAddr ) != 0 );
  while( I2CSentByte( 0x00 ) != 0 );
  while( I2CSentByte( level ) != 0 );
  I2CStop();
}

unsigned char ad5170_get_level(unsigned char addr, unsigned char *level)
{
  uint8 status, slaveAddr;
  slaveAddr = AD5170_I2C_ADDR_R | (AD5170_I2C_ADDR_MASK & (addr << 1));

  I2CStart();
  while( I2CSentByte( slaveAddr ) != 0 );
  *level = I2CReceiveByte(LOW);
  status = I2CReceiveByte(HIGH);
  I2CStop();

  return status;
}
