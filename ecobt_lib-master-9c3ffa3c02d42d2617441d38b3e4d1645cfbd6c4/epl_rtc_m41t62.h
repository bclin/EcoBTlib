
/**
*	@defgroup Module
*	EcoBT main component
*
*/

/**
 *      @ingroup Module
 *      
 *      @defgroup RTC
 *      @{
 *		@file 	epl_rtc_m41t62.h
 *		@brief 	real time clock firmware
 *	
 *		@author	tongkunlai@gmail.com
 *		@date	2012.11.19
 *      @copyright Copyright 2013 EPLAB National Tsing Hua University. All rights reserved.\n
 *      The information contained herein is confidential property of NTHU. 
 *      The material may be used for a personal and non-commercial use only in connection with 
 *      a legitimate academic research purpose.  
 *      Any attempt to copy, modify, and distribute any portion of this source code or derivative thereof for commercial, 
 *      political, or propaganda purposes is strictly prohibited.  
 *      All other uses require explicit written permission from the authors and copyright holders. 
 *      This copyright statement must be retained in its entirety and displayed 
 *      in the copyright statement of derived source code or systems.
 */

#ifndef EPL_RTC_M41T62_H
#define EPL_RTC_M41T62_H

#ifdef __cplusplus
extern "C"
{
#endif

#include "hal_types.h"
#include "epl_hal_i2c.h"
#include "hal_mcu.h"
#include "hal_led.h"

/** An enum describing register map in M41T62.
 */
typedef enum {
	RTC_M41T62_10THS_100THS_OF_SECONDS = 0x00,
	RTC_M41T62_SECONDS = 0x01,
	RTC_M41T62_MINUTES = 0x02,
	RTC_M41T62_HOURS =   0x03,
	RTC_M41T62_DAY =     0x04,
	RTC_M41T62_DATE =    0x05,
	RTC_M41T62_CENTURY_MONTH =  0x06, /* Century = 0~3, Month = 1 ~ 12 */
	RTC_M41T62_YEAR =           0x07,
	RTC_M41T62_CALIBRATION =    0x08,
	RTC_M41T62_WATCHDOG =       0x09,
	RTC_M41T62_ALARM_MONTH =    0x0A,
	RTC_M41T62_ALARM_DATE =     0x0B,
	RTC_M41T62_ALARM_HOUR =     0x0C,
	RTC_M41T62_ALARM_MINUTES =  0x0D,
	RTC_M41T62_ALARM_SECONDS =  0x0E,
	RTC_M41T62_FLAGS =          0x0F
} epl_rtc_m41t62_reg;

/** An enum describing square wave frequency map in M41T62.
 */
typedef enum{
    RTC_M41T52_SQF_NONE = 0x00,
    RTC_M41T52_SQF_32K = 0x10
} epl_rtc_m41t62_SQWF;



/**     @brief  This is will initialize I2C interface and M41T62 RTC
*       @param [in]       void
*/
void rtcInit(void);

/** @brief	Write the Time data to M41T62 RTC
*
* @param [in]       hour
*     hour data to write
* @param [in]       minute
*     minute data to write
* @param [in]       second
*     second data to write
*/
void rtcSetTime( uint8 hour, uint8 minute, uint8 second );


/** @brief	Write Date data to M41T62 RTC
*
* @param [in]              day
*     dat data to write
* @param [in]              date
*     date data to write
* @param [in]              month
*     month data to write
* @param [in]              year
*     year data to write
* @param [in]              century
*     century data to write
*/
void rtcSetDate(uint8 day, uint8 date, uint8 month, uint8 year, uint8 century );

/** @brief	Read Time data to M41T62 RTC
*
* @param [in]       hour
*     parameter for read  hour data of rtc
* @param [in]       minute
*     parameter for read  minute data of rtc 
* @param [in]       second
*     parameter for read  second data of rtc 
*/
void rtcGetTime( uint8* hour, uint8* minute, uint8* second );

/** @brief	Read da data to M41T62 RTC
*
* @param [in]       day
*     parameter for read  day data of rtc
* @param [in]       date
*     parameter for read  date data of rtc
* @param [in]       month
*     parameter for read  month data of rtc
* @param [in]       year
*     parameter for read  year data of rtc
* @param [in]       century
*     parameter for read  century data of rtc
*/
void rtcGetDate(uint8* day, uint8* date, uint8* month, uint8* year);

/** @brief	Read Time and Date data to M41T62 RTC
*
* @param [in]       hour
*     parameter for read  hour data of rtc
* @param [in]       minute
*     parameter for read  minute data of rtc 
* @param [in]       second
*     parameter for read  second data of rtc 
* @param [in]       day
*     parameter for read  day data of rtc
* @param [in]       date
*     parameter for read  date data of rtc
* @param [in]       month
*     parameter for read  month data of rtc
* @param [in]       year
*     parameter for read  year data of rtc
* @param [in]       century
*     parameter for read  century data of rtc
*/
void rtcReadDateAndTime(uint8* hour, uint8* minute, uint8* second, uint8* day
                        , uint8* date, uint8* month, uint8* year);

/** @brief	Enable and write the Time and Date data to the alarm of M41T62 RTC
*
* @param [in]              alarmMonth 	
*     alarmMonth data to write			01~12
* @param [in]              alarmDate 	
*     alarmDate data to write			01~31
* @param [in]              alarmHour 	
*     alarmHour data to write			00~23
* @param [in]              alarmMinutes 	
*     alarmMinutes data to write		00~59
* @param [in]              alarmSeconds 	
*     alarmSeconds data to write		00~59
* @param [in]              repeatMode 	
*     alarm repeat mode data to write
*/
void rtcSetAlarm( uint8 alarmMonth, uint8 alarmDate, uint8 alarmHour
                 , uint8 alarmMinutes, uint8 alarmSeconds, uint8 repeatMode );

/** @brief	read the alarm dat of M41T62 RTC
*
* @param [in]              alarmMonth
*     alarmMonth data to read
* @param [in]              alarmDate
*     alarmDate data to read
* @param [in]              alarmHour
*     alarmHour data to read
* @param [in]              alarmMinutes
*     alarmMinutes data to read
* @param [in]              alarmSeconds
*     alarmSeconds data to read
*/
void rtcGetAlarm( uint8* alarmMonth, uint8* alarmDate, uint8* alarmHour
                 , uint8* alarmMinutes, uint8* alarmSeconds );


/** @brief	read the Time and Date data of the alarm of M41T62 RTC
*
* @return a character pointer.
*/
uint8 rtcGetAlarmStatus( void );

/**	@brief	Config RTC Square Wave.
 *	
 *	@param	[in] freq 
 *		square wave Frequency	
 *	@return	none
 */
void rtcSetSQWF( uint8 freq );

/**	@brief	Enable RTC Square Wave Output.
 *	
 *	@param	none	
 *	@return	none
 */
void rtcEnableSQW( void );

/**
 * @brief   RTC interrupt sevice route
 *
 * @param	alarmEvent
 *		Event Number
 *
 * @return 	None
 */
void halProcessAlarmInterrupt( uint8 alarmEvent );

#ifdef __cplusplus
}
#endif

#endif

/**
@}
*/