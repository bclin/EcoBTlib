
/**	
 *  @ingroup Module
 *  @defgroup epl_rtc_m41t62
 *  @{
 *	@file 	epl_rtc_m41t62.c
 *  
 *
 *	@brief 	Control of the RTC on the EcoBT board.
 *	
 *	@author	tongkunlai@gmail.com
 *	@date	2012.11.19
 */

#include <ioCC2540.h>
#include "hal_types.h"
#include "epl_rtc_m41t62.h"

/*****local variable*****/
#define HAL_RTC_ALARM_BIT    0x01 // 0000 0001
uint8 slaveAddress = 0xd0; // 1101000 0
/************************/


/**     @brief  This is will initialize I2C interface and M41T62 RTC
*       @param [in]       void
*		@return None
*/
void rtcInit(void)
{

  I2CInit();
  
  /*****Alarm interrupt setting*****/
  P0SEL &= 0xFE;    // port 0.0 as GPIO
  P0DIR &= 0xFE;    // port 0.0 as input
  PICTL |= 0x01;    // port interrupt control, 0 rising edge, 1 falling edge
  P0IEN |= 0x01;    // IOC module, prot 0 interrupt mask, 0 interrupts are disabled, 1 interrupt s are enabled.
  P0IE   = 1;       // CPU module, prot 0 interrupt mask, 0 interrupts are disabled, 1 interrupt s are enabled.
  P0IFG  = 0x00;    // P0 interrupt Flag Status
  P0IF   = 0;       // port 0 interrupt status flag
  EA     = 1;       // enable interrupt
  
  /*****RTC setting  *****/
  /*****Start RTC*****/
  I2CStart();
  while( I2CSentByte( slaveAddress ) != 0 );      // slave address 1101000 0 for write   
  while( I2CSentByte( RTC_M41T62_FLAGS ) != 0 );  // pointer to OF( oscillator fail bit) register
  while( I2CSentByte( 0x00 ) != 0 );              // set OF to 0
  I2CStop();	

} //  rtcInit function end

/** @brief	Write the Time data to M41T62 RTC
*
* @param [in]       hour
*     hour data to write
* @param [in]       minute
*     minute data to write
* @param [in]       second
*     second data to write
*/
void rtcSetTime(uint8 hour, uint8 minute, uint8 second )
{
  I2CStart();
  while( I2CSentByte( slaveAddress ) != 0 );	// slave address 1101000 0
  while( I2CSentByte( RTC_M41T62_SECONDS ) != 0 );// seconds register
  while( I2CSentByte( second ) != 0 );            // write second
  while( I2CSentByte( minute ) != 0 );            // write minute
  while( I2CSentByte( hour ) != 0 );		// write hour
  I2CStop();
} // end function rtcSetTime

/** @brief	Read Time data to M41T62 RTC
*
* @param [in]       hour
*     parameter for read  hour data of rtc
* @param [in]       minute
*     parameter for read  minute data of rtc 
* @param [in]       second
*     parameter for read  second data of rtc 
*/
void rtcGetTime( uint8* hour, uint8* minute, uint8* second )
{
  //uint8 slaveAddress = 0xd0; // 1101000 0
  // read sequence data 
  I2CStart();
  while( I2CSentByte( 0xd0 ) != 0 );		// slave address 1101000 0 for write
  while( I2CSentByte(RTC_M41T62_SECONDS ) != 0 );		// second register
  I2CStart();                                     // force re-start
  while( I2CSentByte( 0xd1 ) != 0 ); 		// slave address 1101000 1 for read
  *second = (I2CReceiveByte(LOW)& 0x7F);
  *minute = (I2CReceiveByte(LOW) & 0x7F);
  *hour   =  I2CReceiveByte(HIGH);
  I2CStop();

} // end function rtcGetTime


/** @brief	Write Date data to M41T62 RTC
*
* @param [in]              day
*     dat data to write
* @param [in]              date
*     date data to write
* @param [in]              month
*     month data to write
* @param [in]              year
*     year data to write
* @param [in]              century
*     century data to write
*/
void rtcSetDate(uint8 day, uint8 date, uint8 month, uint8 year, uint8 century )
{
  I2CStart();
  while( I2CSentByte( slaveAddress ) != 0 );	          // slave address 1101000 0
  while( I2CSentByte( RTC_M41T62_DAY ) != 0 );            // pointer to day of week register
  while( I2CSentByte( BUILD_UINT8( 0x00, day) ) != 0 );   // write day,  BUILD_UINT8( RS(square wave output)register, day)
  while( I2CSentByte( ( date & 0x3F ) ) != 0 );           // write day of month
  while( I2CSentByte( ( (century << 6) | month ) ) != 0 );// write month cb1 cb0 0 10M month
                                                          //              0   0  0  0 0000
  while( I2CSentByte(  year ) != 0 );		          // write year
  I2CStop();
} // end function rtcSetDate


/** @brief	Read da data to M41T62 RTC
*
* @param [in]       day
*     parameter for read  day data of rtc
* @param [in]       date
*     parameter for read  date data of rtc
* @param [in]       month
*     parameter for read  month data of rtc
* @param [in]       year
*     parameter for read  year data of rtc
* @param [in]       century
*     parameter for read  century data of rtc
*/
void rtcGetDate(uint8* day, uint8* date, uint8* month, uint8* year )
{
  I2CStart();
  while( I2CSentByte( 0xd0 ) != 0 );		// slave address 1101000 0 for write
  while( I2CSentByte(RTC_M41T62_DAY ) != 0 );	// days register
  I2CStart();                                   // force re-start
  while( I2CSentByte( 0xd1 ) != 0 ); 		// slave address 1101000 1 for read
  *day   = (I2CReceiveByte(LOW ));
  *date  =  I2CReceiveByte(LOW);
  *month = (I2CReceiveByte(LOW) & 0x1F);
  *year  = I2CReceiveByte(HIGH);
  I2CStop();
} // end function rtcGetDate


/** @brief	Read Time and Date data to M41T62 RTC
*
* @param [in]       hour
*     parameter for read  hour data of rtc
* @param [in]       minute
*     parameter for read  minute data of rtc 
* @param [in]       second
*     parameter for read  second data of rtc 
* @param [in]       day
*     parameter for read  day data of rtc
* @param [in]       date
*     parameter for read  date data of rtc
* @param [in]       month
*     parameter for read  month data of rtc
* @param [in]       year
*     parameter for read  year data of rtc
*/
void rtcReadDateAndTime(uint8* hour, uint8* minute, uint8* second, uint8* day
                        , uint8* date, uint8* month, uint8* year)
{
  /*****read sequence rtc data*****/
  I2CStart();
  while( I2CSentByte( slaveAddress ) != 0 );	// slave address 1101000 0 for write
  while( I2CSentByte( 0x01 ) != 0 );			// second register
  I2CStart();
  while( I2CSentByte( 0xd1 ) != 0 ); 			// slave address 1101000 1 for read
  *second = I2CReceiveByte(LOW) & 0x7F;
  *minute = I2CReceiveByte(LOW) & 0x7F; 
  *hour   = I2CReceiveByte(LOW);
  *day    = I2CReceiveByte(LOW);                // day of week
  *date   = I2CReceiveByte(LOW);                // day of month
  *month  = I2CReceiveByte(LOW) & 0x1F;
  *year   = I2CReceiveByte(LOW);
  I2CStop();
  
} // end rtcReadDateAndTime function


/** @brief	Enable and write the Time and Date data to the alarm of M41T62 RTC
*
* @param [in]              alramMonth 
*     alramMonth data to write			01~12
* @param [in]              alarmDate 
*     alarmDate data to write			01~31
* @param [in]              alarmHour 
*     alarmHour data to write			00~23
* @param [in]              alarmMinutes 
*     alarmMinutes data to write		00~59
* @param [in]              alarmSeconds 
*     alarmSeconds data to write		00~59
*/
void rtcSetAlarm( uint8 alarmMonth, uint8 alarmDate, uint8 alarmHour
                  , uint8 alarmMinutes, uint8 alarmSeconds, uint8 repeatMode )
{

  uint8 rpt1 = repeatMode;
  uint8 rpt2 = repeatMode;
  uint8 rpt3 = repeatMode;
  uint8 rpt4 = repeatMode;
  uint8 rpt5 = repeatMode;
  rpt1 = ( rpt1&0x10 ) << 3;  // 0001 0000 >> 1000 0000
  rpt2 = ( rpt2&0x08 ) << 4;  // 0000 1000 >> 1000 0000
  rpt3 = ( rpt3&0x04 ) << 5;  // 0000 0100 >> 1000 0000
  rpt4 = ( rpt4&0x02 ) << 6;  // 0000 0010 >> 1000 0000
  rpt5 = ( rpt5&0x01 ) << 6;  // 0000 0001 >> 0100 0000
  
  I2CStart();
  while( I2CSentByte( slaveAddress ) != 0 );	                  // slave address 1101000 0
  while( I2CSentByte( RTC_M41T62_ALARM_MONTH ) != 0 );    
  while( I2CSentByte( ( 0x80 | alarmMonth ) )  != 0 );            // pointer to Alarm month register  | AFE SQWE 0 AL10M Alarmmonth
                                                                  //       register map                  1   0   0   1     0000 
  while( I2CSentByte( rpt4 + rpt5 + ( 0x3F & alarmDate ) ) != 0 );// write day of month
  while( I2CSentByte( rpt3 + ( 0x3F & alarmHour ) ) != 0 );       // write hour
  while( I2CSentByte( rpt2 + ( 0x7F & alarmMinutes ) ) != 0 );	  // write minute
  while( I2CSentByte( rpt1 + ( 0x7F & alarmSeconds )) != 0 );     // write second
  I2CStop();

  //
  rtcGetAlarmStatus();
} // end function rtcSetAlarm


/** @brief	read the Time and Date data to the alarm of M41T62 RTC* 
*
* @param [in]              alramMonth
*     alramMonth data to read
* @param [in]              alarmDate
*     alarmDate data to read
* @param [in]              alarmHour
*     alarmHour data to read
* @param [in]              alarmMinutes
*     alarmMinutes data to read
* @param [in]              alarmSeconds
*     alarmSeconds data to read
*/
void rtcGetAlarm( uint8* alarmMonth, uint8* alarmDate, uint8* alarmHour
                  , uint8* alarmMinutes, uint8* alarmSeconds )
{
  I2CStart();
  while( I2CSentByte( slaveAddress ) != 0 );          // slave address 1101000 0
  while( I2CSentByte( RTC_M41T62_ALARM_MONTH ) != 0 );   
  I2CStart();                                         // Force restart
  while( I2CSentByte( 0xd1 ) != 0 );                  // slave address 1101000 1
  *alarmMonth =   ( I2CReceiveByte( LOW ) & 0x1F );   // read month of alarm
  *alarmDate =    ( I2CReceiveByte( LOW ) & 0x3F );   // read day of month of alarm
  *alarmHour =    ( I2CReceiveByte( LOW ) & 0x3F );   // read hour of alarm
  *alarmMinutes = ( I2CReceiveByte( LOW ) & 0x7F );   // read minute of alarm
  *alarmSeconds = ( I2CReceiveByte( LOW ) & 0x7F );   // read seconds of alarm
  I2CReceiveByte( HIGH );                             // read Alarm Flag
  I2CStop();
} // end function rtcGetAlarm


/** @brief	read the Time and Date data of the alarm of M41T62 RTC
*	@param none
* 	@return a character pointer.
*/
uint8 rtcGetAlarmStatus( void )
{
  uint8 result = 0xff;
  I2CStart();
  while( I2CSentByte( slaveAddress ) != 0 );          // slave address 1101000 0
  while( I2CSentByte( RTC_M41T62_FLAGS ) != 0 );   
  I2CStart();                                         // Force restart
  while( I2CSentByte( 0xd1 ) != 0 );                  // slave address 1101000 1
  result = I2CReceiveByte( HIGH );                    // read Alarm Flag
  I2CStop();
   
  return result;
} // end function rtcGetAlarmStatus


/**	@brief	Config RTC Square Wave.
 *	
 *	@param	[in] freq
 *		square wave frequency to config
 *	@return	none
 */
void rtcSetSQWF( uint8 freq )
{
  uint8 result = 0xff;
  
  // read the origin data
  I2CStart();
  while( I2CSentByte( slaveAddress ) != 0 );          // slave address 1101000 0
  while( I2CSentByte( RTC_M41T62_DAY ) != 0 );   
  I2CStart();                                         // Force restart
  while( I2CSentByte( 0xd1 ) != 0 );                  // slave address 1101000 1
  result = I2CReceiveByte( HIGH );                    // read Alarm Flag
  I2CStop();
  
  result = result & 0x01;
  
  freq = freq | result;
  
  I2CStart();
  while( I2CSentByte( slaveAddress ) != 0 );	                  // slave address 1101000 0
  while( I2CSentByte( RTC_M41T62_DAY ) != 0 );    
  while( I2CSentByte( freq ) );// write day of month
  I2CStop();
  
}

/**	@brief	Enable RTC Square Wave Output.
 *	
 *	@param	none	
 *	@return	none
 */
void rtcEnableSQW( void )
{
  uint8 result = 0xff;
  
  // read the origin data
  I2CStart();
  while( I2CSentByte( slaveAddress ) != 0 );          // slave address 1101000 0
  while( I2CSentByte( RTC_M41T62_ALARM_MONTH ) != 0 );   
  I2CStart();                                         // Force restart
  while( I2CSentByte( 0xd1 ) != 0 );                  // slave address 1101000 1
  result = I2CReceiveByte( HIGH );                    // read Alarm Flag
  I2CStop();
  
  result = result | 0x40;
  
  I2CStart();
  while( I2CSentByte( slaveAddress ) != 0 );	                  // slave address 1101000 0
  while( I2CSentByte( RTC_M41T62_ALARM_MONTH ) != 0 );    
  while( I2CSentByte( result ) );// write day of month
  I2CStop();
}

/**
 * @brief   RTC interrupt sevice route
 *
 * @param	[in] alarmEvent Event Number
 *
 * @return 	None
 */
void halProcessAlarmInterrupt( uint8 alarmEvent )
{
 HalLedSet(HAL_LED_1, HAL_LED_MODE_TOGGLE);
}
 

/***************************************************************************************************
 *                                    INTERRUPT SERVICE ROUTINE
 ***************************************************************************************************/

/**
 * @brief   Create RTC interrupt sevice route
 *
 * @param	None
 *
 * @return 	None
 */
HAL_ISR_FUNCTION( halAlarmPort0Isr, P0INT_VECTOR )
{
  HalLedSet(HAL_LED_2, HAL_LED_MODE_TOGGLE);
  if ( P0IFG & HAL_RTC_ALARM_BIT )
  {
    halProcessAlarmInterrupt(1);
  }

  /*
    Clear the CPU interrupt flag for Port_0
    PxIFG has to be cleared before PxIF
  */
  P0IFG = 0;
  P0IF = 0;
}

/**
@}
*/
