#ifndef EPL_LED_H
#define EPL_LED_H

#include <ioCC2540.h>

#define LED0 P2_1
#define LED1 P2_2

#define LED_ACTIVE 0

#define LED0_ACTIVE LED_ACTIVE
#define LED1_ACTIVE LED_ACTIVE

#define epl_led_init()                                                        \
{                                                                             \
  /* setup P2 GPIO pin 1,2 for LEDs */                                        \
  P2DIR |= 0x06;                                                              \
  /* turn off all LEDs */                                                     \
  epl_led0_off();                                                             \
  epl_led1_off();                                                             \
}

#define epl_ledGreen_on() st( LED1 = LED1_ACTIVE; )
#define epl_ledGreen_off() st( LED1 = !LED1_ACTIVE; )
#define epl_ledGreen_toggle() st( if (LED1) { LED1 = 0; } else { LED1 = 1;} )

#define epl_ledRed_on() st( LED0 = LED0_ACTIVE; )
#define epl_ledRed_off() st( LED0 = !LED0_ACTIVE; )
#define epl_ledRed_toggle() st( if (LED0) { LED0 = 0; } else { LED0 = 1;} )

#define epl_led0_on() st( LED0 = LED0_ACTIVE; )
#define epl_led1_on() st( LED1 = LED0_ACTIVE; )
#define epl_leds_on() st( P2 |= 0x06; )

#define epl_led0_off() st( LED0 = !LED0_ACTIVE; )
#define epl_led1_off() st( LED1 = !LED1_ACTIVE; )
#define epl_leds_off() st( P2 ~= !(0x06); )

#define epl_led0_toggle() st( if (LED0) { LED0 = 0; } else { LED0 = 1;} )
#define epl_led1_toggle() st( if (LED1) { LED1 = 0; } else { LED1 = 1;} )

#endif